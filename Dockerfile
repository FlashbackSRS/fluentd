# Copy value from https://github.com/kubernetes/kubernetes/blob/master/cluster/addons/fluentd-elasticsearch/fluentd-es-ds.yaml
FROM quay.io/fluentd_elasticsearch/fluentd:v2.7.0

# Copied from https://github.com/sekka1/kubernetes-fluentd-loggly/blob/master/build.sh#L37
# install the fluent-plugin-loggly gem from Ruby Gems or from GitHub
RUN gem install fluent-plugin-loggly -v 0.0.9
